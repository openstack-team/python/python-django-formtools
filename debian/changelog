python-django-formtools (2.4.1-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090490).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 11:07:32 +0100

python-django-formtools (2.4.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 21:31:06 +0200

python-django-formtools (2.4.1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Aug 2023 14:02:27 +0200

python-django-formtools (2.3-3) unstable; urgency=medium

  * Use /usr/bin/django-admin instead of django-admin.py. (Closes: #1013605).

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Jun 2022 15:59:14 +0200

python-django-formtools (2.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * New upstream release.
  * Removed add-use_required_attribute-false-in-test.patch applied upstream.
  * Set SETUPTOOLS_SCM_PRETEND_VERSION and build-depends on
    python3-setuptools-scm.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Feb 2022 09:52:45 +0100

python-django-formtools (2.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * Removed Python 2 support.
  * New upstream release.
  * Django 2.2 compat:
    add-use_required_attribute-false-in-test.patch

 -- Thomas Goirand <zigo@debian.org>  Wed, 10 Jul 2019 11:42:01 +0200

python-django-formtools (2.0-1) unstable; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release:
    - Do not use lazy_property anymore (Closes: #865944).

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Aug 2017 15:02:32 +0000

python-django-formtools (1.0+20160714.git54f1ccca01-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https

  [ Thomas Goirand ]
  * New upstream release based on git 1.0+20160714.git54f1ccca01
    (Closes: #828667).
  * Fixed debian/copyright ordering.
  * Standards-Version: 3.9.8 (no change).
  * Using pkgos-dh_auto_install from openstack-pkg-tools >= 52~.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Jul 2016 09:20:46 +0000

python-django-formtools (1.0-2) unstable; urgency=medium

  * Fix FTBFS with Python 3 (Closes: #803705).

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Nov 2015 11:02:11 +0000

python-django-formtools (1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #798669)

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Sep 2015 17:47:46 +0200
